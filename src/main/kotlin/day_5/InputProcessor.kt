package day_5

import java.util.*

object InputProcessor {
    fun process9000(): String {
        val input = FileProcessor.mapInput()
        val stacks = toStacks(input).stacks
        input.toInstructions()
            .forEach {
                for (i in 1..it.first) {
                    stacks[it.second] pushTo stacks[it.third]
                }
            }
        return stacks.map { it.first() }.joinToString("")
    }

    fun processOver9000(): String {
        val input = FileProcessor.mapInput()
        val stacks = toStacks(input).stacks
        val movedCrates = mutableListOf<Char>()
        input.toInstructions()
            .forEach {instruction ->
                for (i in 1..instruction.first) {
                    movedCrates.add(stacks[instruction.second].pop())
                }
                movedCrates.reversed().forEach { stacks[instruction.third].push(it) }
                movedCrates.clear()
            }
        return stacks.map { it.first() }.joinToString("")
    }
}

private infix fun ArrayDeque<Char>.pushTo(other: ArrayDeque<Char>) =
    other.push(this.pop())