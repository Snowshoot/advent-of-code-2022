package day_5

import java.util.ArrayDeque

data class Stacks(
    val stacks: List<ArrayDeque<Char>> = listOf(
        ArrayDeque<Char>(),
        ArrayDeque<Char>(),
        ArrayDeque<Char>(),
        ArrayDeque<Char>(),
        ArrayDeque<Char>(),
        ArrayDeque<Char>(),
        ArrayDeque<Char>(),
        ArrayDeque<Char>(),
        ArrayDeque<Char>()
    )
) {
    fun fill(chars: List<Char>) =
        this.stacks.apply {
            chars.forEachIndexed { idx, char ->
                stacks[idx].add(char)
            }
        }.removeEmptyChars()

    private fun List<ArrayDeque<Char>>.removeEmptyChars() =
        this.apply {
            stacks.forEach {
                it.removeIf { char -> char == ' ' }
            }
        }
}

fun String.toChars() =
    this.mapIndexedNotNull { idx, it ->
        if ((idx + 3) % 4 == 0)
            it
        else
            null
    }