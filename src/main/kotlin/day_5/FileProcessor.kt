package day_5

object FileProcessor {
    fun mapInput() =
        FileLoader
            .load("/day_5/input.txt")
            .readText()
            .split("\n")
}

fun toStacks(strings: List<String>) =
    Stacks().apply {
        strings.take(8).forEach {
            this.fill(it.toChars())
        }
    }

fun List<String>.toInstructions(): List<Triple<Int, Int, Int>> =
    this.takeLast(501).map {
        Triple(
            it.substringAfter("move ").substring(0, 2).removeSpaces().toInt(),
            it.substringAfter(" from ").first().digitToInt() -1,
            it.substringAfter(" to ").first().digitToInt() -1
        )
    }

private fun String.removeSpaces() = this.run { replace(" ", "") }