package day_2

import day_2.enums.*

object ScoreCounter {
    fun getScore(): Int =
        FileProcessor.mapInput()
            .map { symbols ->
            symbols.first.toShape() to symbols.second.toShape()
        }.sumScore()

    fun getScoreMappedByResult(): Int =
        FileProcessor.mapInput()
            .map { symbols ->
                symbols.first.toShape() to symbols.second.toResult()
            }.sumScoreByResult()
}

fun List<Pair<Shape, Shape>>.sumScore(): Int =
    this.sumOf {
        it.second.toScore() + it.toResult().toScore()
    }

fun List<Pair<Shape, Result>>.sumScoreByResult(): Int =
    this.sumOf {
        it.second.toScore() + it.toShapeNeededForResult().toScore()
    }
