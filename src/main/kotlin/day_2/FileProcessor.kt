package day_2

object FileProcessor {
    fun mapInput() =
        FileLoader
            .load("/day_2/input.txt")
            .readText()
            .split("\n")
            .map { it.split(" ").let { it[0] to it[1] } }
}