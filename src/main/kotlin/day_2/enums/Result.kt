package day_2.enums

enum class Result {
    won, lost, draw
}

fun Pair<Shape, Shape>.toResult(): Result {
    if (this.first == this.second) return Result.draw
    return when (this.first) {
        Shape.rock -> (this.second == Shape.paper).toResult()
        Shape.paper -> (this.second == Shape.scissors).toResult()
        Shape.scissors -> (this.second == Shape.rock).toResult()
    }
}

fun Pair<Shape, Result>.toShapeNeededForResult(): Shape {
    if (this.second == Result.draw) return this.first
    return when (this.first) {
        Shape.rock -> this.second.getShape(Shape.paper, Shape.scissors)
        Shape.paper -> this.second.getShape(Shape.scissors, Shape.rock)
        Shape.scissors -> this.second.getShape(Shape.rock, Shape.paper)
    }
}

fun String.toResult() =
    when (this) {
        "X" -> Result.lost
        "Y" -> Result.draw
        "Z" -> Result.won
        else -> throw Exception("Broken input")
    }

private fun Result.getShape(shapeToWin: Shape, shapeToLose: Shape) =
    if (this == Result.won)
        shapeToWin
    else shapeToLose

fun Boolean.toResult() =
    if (this) Result.won
    else Result.lost

fun Result.toScore() =
    when (this) {
        Result.won -> 6
        Result.draw -> 3
        Result.lost -> 0
    }