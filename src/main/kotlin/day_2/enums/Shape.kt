package day_2.enums

enum class Shape {
    rock, paper, scissors
}

fun String.toShape() =
        when (this) {
            "A", "X" -> Shape.rock
            "B", "Y" -> Shape.paper
            "C", "Z" -> Shape.scissors
            else -> throw Exception("Broken input")
        }

fun Shape.toScore() =
    when(this) {
        Shape.rock -> 1
        Shape.paper -> 2
        Shape.scissors -> 3
    }