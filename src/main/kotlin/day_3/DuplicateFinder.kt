package day_3

fun findDuplicatedItems() =
    FileProcessor.mapInput().splitBackpackCompartments()
        .map { backpack ->
            (backpack.first findDuplicates backpack.second).first()
        }

fun findDuplicatedBadges() =
    FileProcessor.mapInput().splitToElfGroups()
        .map {
            (it.first findDuplicates it.second).toString()
                .findDuplicates(it.third).first()
        }

private fun List<String>.splitBackpackCompartments() =
    this.map {
        it.substring(0, (it.length / 2)) to
                it.substring(it.length / 2)
    }

private fun List<String>.splitToElfGroups() =
    this.chunked(3)
        .map { group -> Triple(group[0], group[1], group[2]) }

private infix fun String.findDuplicates(other: String): List<Char> =
    this.mapNotNull {
        if (other.contains(it))
            it
        else
            null
    }
