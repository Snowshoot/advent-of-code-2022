package day_3


object PriorityCounter {
    fun getPrioritySumOfItems() =
        findDuplicatedItems()
            .sumOf { it.toItemPriority() }

    fun getPrioritySumOfBadges() =
        findDuplicatedBadges()
            .sumOf { it.toItemPriority() }
}

fun Char.toItemPriority() =
    if (this.isUpperCase())
        this.code - asciiUpperCaseA + asciiUpperCaseAPriority
    else
        this.code - asciiLowerCaseA + asciiLowerCaseAPriority

const val asciiUpperCaseA = 65
const val asciiUpperCaseAPriority = 27
const val asciiLowerCaseA = 97
const val asciiLowerCaseAPriority = 1
