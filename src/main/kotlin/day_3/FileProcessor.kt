package day_3

object FileProcessor {
    fun mapInput() =
        FileLoader
            .load("/day_3/input.txt")
            .readText()
            .split("\n")
}