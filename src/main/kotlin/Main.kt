import Colors.ANSI_BLUE
import Colors.ANSI_PURPLE
import Colors.ANSI_RESET
import Colors.YELLOW_BOLD_BRIGHT
import day_1.CalorieCounter
import day_2.ScoreCounter
import day_3.PriorityCounter
import day_4.CleaningRangeComparator
import day_5.InputProcessor

fun main(args: Array<String>) {
    listOf(
        CalorieCounter.getTastiestElf() to CalorieCounter.getThreeTastiestElves(),
        ScoreCounter.getScore() to ScoreCounter.getScoreMappedByResult(),
        PriorityCounter.getPrioritySumOfItems() to PriorityCounter.getPrioritySumOfBadges(),
        CleaningRangeComparator.getContainedPairCount() to CleaningRangeComparator.getOverlappingPairCount(),
        InputProcessor.process9000() to InputProcessor.processOver9000(),
    ).printAllAnswers()
}

private fun <X, D> Collection<Pair<X, D>>.printAllAnswers() =
    this.forEachIndexed { idx, answers ->
        printAndResetColor("Day Number: $ANSI_BLUE${idx + 1}")
        printAndResetColor("Answer for 1st $goldStar: $ANSI_BLUE${answers.first}")
        printAndResetColor("Answer for 2nd $goldStar: $ANSI_BLUE${answers.second}")
        printAndResetColor(ANSI_PURPLE + "-".repeat(40))
    }

private fun printAndResetColor(text: String) =
    println(text).also { print(ANSI_RESET) }

private val goldStar
    get() =
        "$YELLOW_BOLD_BRIGHT*$ANSI_RESET"

object Colors {
    const val ANSI_RESET = "\u001B[0m"
    const val ANSI_BLUE = "\u001B[34m"
    const val ANSI_PURPLE = "\u001B[35m"
    const val YELLOW_BOLD_BRIGHT = "\u001b[1;93m"
}