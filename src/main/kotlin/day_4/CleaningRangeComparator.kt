package day_4

object CleaningRangeComparator{
    fun getContainedPairCount() = inputInPairs().findContainingRanges()
    fun getOverlappingPairCount() = inputInPairs().findOverlapping()

    private fun List<Pair<IntRange, IntRange>>.findContainingRanges() =
        this.sumOf { ranges ->
            if (ranges.second.all { ranges.first.contains(it) } || ranges.first.all { ranges.second.contains(it) })
                1.toInt()
            else
                0.toInt()
        }

    private fun List<Pair<IntRange, IntRange>>.findOverlapping() =
        this.sumOf { range ->
            if (range.first.contains(range.second.min()) || range.first.contains(range.second.max()) ||
                range.second.contains(range.first.min()) || range.second.contains(range.first.max()))
                1.toInt()
            else
                0.toInt()
        }
}