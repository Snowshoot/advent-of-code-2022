package day_4

object FileProcessor {
    fun mapInput() =
        FileLoader
            .load("/day_4/input.txt")
            .readText()
            .split("\n")
}