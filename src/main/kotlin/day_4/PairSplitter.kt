package day_4

fun inputInPairs() =
    FileProcessor.mapInput().map { line ->
        line.split(",")
            .let { it[0] to it[1] }
    }.map {
        it.first.toRange() to
        it.second.toRange()
    }

private fun String.toRange() =
    this.split("-")
        .let { IntRange(it[0].toInt(), it[1].toInt()) }
