package day_1

object CalorieCounter {
    private val data = FileProcessor.mapInput()

    fun getTastiestElf() =
        data.max()

    fun getThreeTastiestElves() =
        data.sortedDescending()
            .take(3)
            .sum()
}