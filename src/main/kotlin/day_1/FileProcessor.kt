package day_1

import FileLoader

object FileProcessor {
    fun mapInput(): List<Int> =
        FileLoader
            .load("/day_1/input.txt")
            .readText()
            .split("\n\n")
            .map { it.split("\n").sumOf { caloriesText -> caloriesText.toInt() } }
}