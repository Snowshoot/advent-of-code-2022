import java.io.FileNotFoundException

object FileLoader {
    fun load(path: String) =
        FileLoader::class.java.getResource(path) ?: throw FileNotFoundException("shid")
}